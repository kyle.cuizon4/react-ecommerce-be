const express = require("express");
const router = express.Router();

//middlewares
const { authCheck, adminCheck } = require("../middlewares/auth");

//controllers
const {userCart, 
    getUserCart, 
    emptyCart, 
    saveAddress, 
    applyCouponToUserCart, 
    createOrder, 
    orders, 
    addToWishlist, 
    wishlist,
    createCashOrder, 
    removeFromWishlist} = require('../controllers/user')

router.post("/user/cart", authCheck, userCart); // save the user cart
router.get("/user/cart", authCheck, getUserCart) //get cart
router.delete("/user/cart", authCheck, emptyCart) //empty cart
router.post('/user/address', authCheck, saveAddress); // save adddress

router.post('/user/order', authCheck, createOrder); //stripe
router.post('/user/cash-order', authCheck, createCashOrder) //cash on delivery
// coupon
router.post('/user/cart/coupon', authCheck, applyCouponToUserCart )

router.get('/user/orders', authCheck, orders)


router.post('/user/wishlist', authCheck, addToWishlist)
router.get('/user/wishlist', authCheck, wishlist)
router.put('/user/wishlist/:productId', authCheck, removeFromWishlist)


// router.get('/user', (req,res) => {
//     //req res handling
//     res.json({
//         data : 'Hello, im user api endpoint!'
//     });
// });



module.exports = router;
