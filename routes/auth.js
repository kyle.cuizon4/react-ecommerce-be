const express = require("express");
const router = express.Router();

//middlewares
const {authCheck, adminCheck} = require('../middlewares/auth')

//import
const {createOrUpdateUser, currentUser} = require('../controllers/auth')

// route (methods)
router.post('/create-or-update-user', authCheck, createOrUpdateUser);
router.post('/current-user', authCheck, currentUser)
//admin route protector
router.post('/current-admin', authCheck,adminCheck, currentUser)

module.exports = router;
