const express = require("express");
const router = express.Router();

//middlewares
const {authCheck, adminCheck} = require('../middlewares/auth')

//import
const {create, read, update, remove, list, getSubs} = require('../controllers/category')

// route
 // create category

router.post('/category', authCheck, adminCheck, create);

// get all categories
router.get('/categories', list);

//get single category

router.get('/category/:slug', read);

//update

router.put('/category/:slug', authCheck, adminCheck, update);

//delete
router.delete('/category/:slug', authCheck, adminCheck, remove);


router.get('/category/subs/:_id', getSubs)

module.exports = router;
