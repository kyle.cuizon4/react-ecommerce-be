
const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')

//filesystem from node
const {readdirSync} = require('fs')
require('dotenv').config()


//import routes

// const authRoutes = require('./routes/auth')

//application
const app = express()

//db -- additional configuration options
mongoose.connect(process.env.DATABASE, {
    useNewUrlParser : true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology : true
    
})
.then(() => console.log(`DB Connected`))
.catch(err => console.log(`DB Connection Error ${err}`))

//middlewares - routes

app.use(morgan("dev"));
app.use(bodyParser.json({limit : "2mb"}));
app.use(cors());


//route middleware
    // app.use('/api', authRoutes)
//auto load routes
readdirSync('./routes').map((r) => app.use('/api', require('./routes/' + r)))

//port 
const port = process.env.PORT || 8000;

app.listen(port, () => console.log(`Server is running on port ${port}`));


