const User = require("../models/user");
const Cart = require("../models/cart");
const Product = require("../models/product");
const Coupon = require("../models/coupon");
const stripe = require("stripe")(process.env.STRIPE_SECRET);

exports.createPaymentIntent = async (req, res) => {
  // later apply coupon
  // later calculate price
  // console.log('coupon body ========>', req.body)
  const {couponApplied} = req.body
  
  //find user
  const user = await User.findOne({email : req.user.email}).exec()

  // get user cart total
  const {cartTotal, totalAfterDiscount} = await Cart.findOne({orderdBy : user._id}).exec()

    let finalAmount = 0
  //charge - create payment intent with order amount and currency

  if(couponApplied && totalAfterDiscount) {
    finalAmount =  totalAfterDiscount * 100
  } else {
    finalAmount = cartTotal * 100
  }

  const paymentIntent = await stripe.paymentIntents.create({
    amount: finalAmount,
    currency: "php",
  });

  res.send({
    clientSecret: paymentIntent.client_secret,
    cartTotal,
    totalAfterDiscount,
    payable: finalAmount
  });
};
