const Coupon = require('../models/coupon.js')



exports.create = async (req,res) => {

    try {

        const {name, expiry, discount} = req.body.coupon

        const coupon = await new Coupon({name, expiry, discount}).save()
        
        res.json(coupon)
        
    } catch (error) {
        console.log(error)
    }


}

exports.remove = async (req,res) => {

    
    try {
        
        const coupon = await Coupon.findByIdAndDelete(req.params.couponId).exec()
        
        res.json(coupon)

    } catch (error) {
        console.log(error)
    }


}


exports.list = async (req,res) => {
        
    try {
        
        const coupon = await Coupon.find({}).sort({createdAt: -1}).exec()
        
        res.json(coupon)

    } catch (error) {
        console.log(error)
    }


}