const Product = require("../models/product");
const slugify = require("slugify");
const User = require("../models/user");

exports.create = async (req, res) => {
  try {
    console.log(req.body);
    req.body.slug = slugify(req.body.title);
    const newProduct = await new Product(req.body).save();
    res.json(newProduct);
  } catch (err) {

    console.log(err);
    // res.status(400).send("Create product failed");
    res.status(400).json({
      err: err.message,
    });
  }
};

exports.listAll = async (req, res) => {
  let products = await Product.find({})
    .limit(parseInt(req.params.count))
    .populate("category")
    .populate("subs")
    .sort([["createdAt", "desc"]])
    .exec();
  res.json(products);
};


exports.remove = async (req, res) => {
  try {
    const deleted = await Product.findOneAndRemove({
      slug: req.params.slug,
    }).exec();
    res.json(deleted);
  } catch (err) {
    console.log(err);
    return res.status(400).send("Product delete failed");
  }
};


exports.read = async (req, res) => {
  const product = await Product.findOne({ slug: req.params.slug })
    .populate("category")
    .populate("subs")
    .exec();
  res.json(product);
};

exports.update = async (req, res) => {
  try {
    if (req.body.title) {
      req.body.slug = slugify(req.body.title)
    }
    const updated = await Product.findOneAndUpdate({slug : req.params.slug}, req.body,
      {new : true}).exec()
      res.json(updated)
  } catch (err) {
      console.log('Product update error', err)
      return res.status(400).json({
        err: err.message,
      });
  }

}

// exports.list = async (req,res) => {
//   try {
//     //createdAt / updatedAt , desc/asc, 3

//     const {sort, order, limit} = req.body
//     const products = await Product.find({})
//     .populate('category')
//     .populate('subs')
//     .sort([[sort, order]])
//     .limit(limit)
//     .exec();

//     res.json(products)

//   } catch (error) {
//     console.log(error)
//   }
// }

//WITH PAGINATION
  exports.list = async (req,res) => {
    try {
      //createdAt / updatedAt , desc/asc, 3

      const {sort, order, page} = req.body
      const currentPage = page || 1
      const perPage = 3

      const products = await Product.find({})
      .skip((currentPage - 1 ) * perPage)
      .populate('category')
      .populate('subs')
      .sort([[sort, order]])
      .limit(perPage)
      .exec();

      res.json(products)

    } catch (error) {
      console.log(error)
    }
  }


exports.productsCount = async (req,res) => {
  let total = await Product.find({}).estimatedDocumentCount().exec();
  res.json(total)
}

exports.productStar = async ( req , res) => {
  let product = await Product.findById(req.params.productId).exec()
  const user = await User.findOne({email : req.user.email}).exec()
  const {star} = req.body

// who is updating?
//check if currently logged in user has already added rating to this product
  let existingRatingObject = product.ratings.find((ele) => (ele.postedBy.toString() === user._id.toString()))

  // if user hasn't left rating yet, push it to product rating array
  if(existingRatingObject === undefined) {

    let ratingAdded = await Product.findByIdAndUpdate(product._id, {

      $push : { 
        ratings: {
          star : star,
          postedBy : user._id
        } 
      }
    }, {new: true})
    console.log('ratingAdded', ratingAdded);
    res.json(ratingAdded);
  } else {
    //if user has already left rating, update it
    const ratingUpdated = await Product.updateOne({
      ratings : {$elemMatch : existingRatingObject} 
    }, {$set : {'ratings.$.star': star}}, {new: true})
    .exec()
    console.log(ratingUpdated)
    res.json(ratingUpdated)

  }

} 


exports.listRelated = async(req,res) => {
  const {productId, sort, order, page} = req.body

  const currentPage = page || 1
  const perPage = 3

  const product = await Product.findById(productId).exec()

  const related = await Product.find({
    _id : {$ne : product._id},
    category : product.category
  })
  .skip((currentPage - 1 ) * perPage)
  .populate('category')
  .populate('subs')
  .populate('postedBy')
  .sort([[sort, order]])
  .limit(perPage)
  .exec()

  res.json(related);
}

exports.productsRelatedCount = async(req,res) => {

  // const {productId} = req.params.productId

  const product = await Product.findById(req.params.productId).exec()

  const related = await Product.find({
    _id : {$ne : product._id},
    category : product.category
  })
  .count()
  .exec()

  res.json(related)
}

// -------------------------------   HELPER METHODS FOR SEARCH 
// -------------------------------  CREATE SERPARATE MODULE  FOR HELPERS
const handleQuery = async(req, res, query) => {
  const products = await Product.find({$text : {$search : query}})
  .populate('category', '_id name')
  .populate('subs', '_id name')
  .populate('postedBy', '_id name')
  .exec()

  res.json(products)

}

const handlePrice = async(req,res, price) => {
  
  try {
      let products = await Product.find({
        price: {
          $gte : price[0],
          $lte : price[1]
        }
      })
      .populate('category', '_id name')
      .populate('subs', '_id name')
      .populate('postedBy', '_id name')
      .exec()

      res.json(products)

  } catch (err) {
    console.log(err);
  }
}

const handleCategory = async(req,res,category) => {
  console.log('Category 2!!!!!!!!!!! ===>', category)
  try {
    let products = await Product.find({category : category})
    .populate('category', '_id name')
    .populate('subs', '_id name')
    .populate('postedBy', '_id name')
    .exec()

    res.json(products)
    console.log(products)
  } catch (error) {
    console.log(error)
  }

}

const handleStar =  (req, res, stars) => {
    Product.aggregate([
      {
        $project : {
          document : "$$ROOT",
          floorAverage : {
            $floor : {$avg : '$ratings.star'}
          }
        }
      },
      { $match : {floorAverage : stars}}
    ])
    .limit(12)
    .exec((err, aggregate) => {
      if(err) console.log('Aggregate ERROR', err)
      Product.find({_id : aggregate})
      .populate('category', '_id name')
      .populate('subs', '_id name')
      .populate('postedBy', '_id name')
      .exec((err, products) => {
        if(err) console.log('PRODUCT AGGREGATE ERROR', err)
        res.json(products)
      })
    })
}

const handleSub = async(req,res,sub) => {
  const products = await Product.find({subs : sub})
  .populate('category', '_id name')
  .populate('subs', '_id name')
  .populate('postedBy', '_id name')
  .exec();

  res.json(products);

}

const handleShipping = async (req, res, shipping) => {
  const products = await Product.find({shipping : shipping})
  .populate('category', '_id name')
  .populate('subs', '_id name')
  .populate('postedBy', '_id name')
  .exec();

  res.json(products);

}
const handleColor = async (req, res, color) => {
  const products = await Product.find({color : color})
  .populate('category', '_id name')
  .populate('subs', '_id name')
  .populate('postedBy', '_id name')
  .exec();

  res.json(products);
  
}
const handleBrand = async (req, res, brand) => {
  const products = await Product.find({brand : brand})
  .populate('category', '_id name')
  .populate('subs', '_id name')
  .populate('postedBy', '_id name')
  .exec();

  res.json(products);
  
}

// ---------------------------------

exports.searchFilters = async (req, res) => {


  const {query, price, category, stars, sub, shipping, color, brand} = req.body

  if(query) {
    console.log('query', query)
    await handleQuery(req, res, query)
  }

  //example : price [20,200]
  if(price !== undefined){
    console.log('Price ===>' , price)
    await handlePrice(req,res,price)
  }

  if(category){
    
    await handleCategory(req,res,category)
  }

  if(stars) {
    console.log('Stars ====>' , stars)

    await handleStar(req,res,stars)
  }

  if(sub){
    console.log('sub ====>' , sub)

    await handleSub(req,res,sub)
  }

  if(shipping) {
     console.log('Shipping ====> ', shipping);
     await handleShipping(req,res, shipping)
  }

  if(color) {
    console.log('Color ====> ', color);
    await handleColor(req,res, color)
 }
  if(brand) {
  console.log('Brand ====> ', brand);
  await handleBrand(req,res, brand)
}


}