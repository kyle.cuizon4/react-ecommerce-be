const Category = require('../models/category');
const Product = require('../models/product')
const Sub = require('../models/sub')
const slugify = require('slugify')

exports.create = async (req,res) => {
    //
    try {
        console.log(req.body)
        const {name} = req.body
        const category = await new Category({name : name , slug : slugify(name)}).save();
        if(category) {
            res.json(category)
        } else {
            res.status(400).send('Something went wrong.')
        }

    } catch (error) {
        console.log(error)
        res.status(400).send('Create category failed. Duplicate category will result in failure.')
    }
}

exports.list = async (req,res) => {
    //
    res.json(await Category.find({}).sort({createdAt : -1}).exec())
}
exports.read = async (req,res) => {
    //
    let category = await Category.findOne({slug: req.params.slug}).exec()
    
    const products = await Product.find({category})
    .populate('category')
    .exec();

    res.json({
        category,
        products
    })
}
exports.update = async (req,res) => {
    //
    const {name} = req.body;
    try {
        const updated = await Category.findOneAndUpdate(
            {slug : req.params.slug}, 
            {name : name, slug : slugify(name)},
            // will send updated document instead of found one.
            {new: true}
            );

        res.json(updated)
        
    } catch (error) {
        res.status(400).send('Update failed')
    }
}
exports.remove = async (req,res) => {
    try {   

        const deleted =  await Category.findOneAndDelete({slug : req.params.slug})
        res.json(deleted);
        
    } catch (error) {
        res.status(400).send('Delete failed')
    }
    //
}

exports.getSubs = (req, res) => {
    Sub.find({parent: req.params._id}).exec((err, subs) => {
        if(err) console.log(err)
        res.json(subs)
    })
}   

